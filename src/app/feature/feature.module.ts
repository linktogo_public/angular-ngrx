import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StartModule } from './start/start.module';
import { HelloModule } from './hello/hello.module';



@NgModule({
  imports: [
    CommonModule,
    StartModule,
    HelloModule,
  ],
  exports: [
    CommonModule,
    StartModule,
    HelloModule,
  ]
})
export class FeatureModule { }