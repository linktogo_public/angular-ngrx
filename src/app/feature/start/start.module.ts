import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StartComponent } from './start.component';



@NgModule({
  declarations: [StartComponent],
  imports: [
    CommonModule
  ]
})
export class StartModule { }
